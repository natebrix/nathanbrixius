# Eigenvector centrality NCAA basketball ratings
# Nathan Brixius, 3/17/2014
#
# Based on an idea from:
#  http://blog.biophysengr.net/2012/03/eigenbracket-2012-using-graph-theory-to.html
# combined with the Sokol 2010 win probability model described here:
#  http://netprophetblog.blogspot.com/2011/06/logistic-regressionmarkov-chain-lrmc.html
# Plus weighting for recency

import csv
import numpy as np
import operator
import numpy.linalg as LA
import datetime as dt
import random

# CDF of standard normal distribution.
# Thanks John D. Cook:
# http://www.johndcook.com/python_phi.html
def phi(x):
    import math
    # constants
    a1 =  0.254829592
    a2 = -0.284496736
    a3 =  1.421413741
    a4 = -1.453152027
    a5 =  1.061405429
    p  =  0.3275911
    
    # Save the sign of x
    sign = 1
    if x < 0:
        sign = -1
    x = abs(x)/math.sqrt(2.0)
    
    # A&S formula 7.1.26
    t = 1.0/(1.0 + p*x)
    y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*math.exp(-x*x)
    
    return 0.5*(1.0 + sign*y)

# The Win Probability model
def rh(x):
	return phi(0.0189 * x - 0.0756)

def getD1(teamsCsv):
    div=[]
    with open(teamsCsv, 'rU') as csvfile:
        r = csv.reader(csvfile)
        for row in r:
	   div.append(row)

        div1 = set()
        for d in div:
	   if d[5]=='I':
		div1.add(d[0])
    return div1

def readGamesMassey(ncaaCsv, dateformat, stdev):
    games=[]
    with open(ncaaCsv, 'rU') as csvfile:
	r = csv.reader(csvfile)
	for row in r:
	   date = dt.datetime.strptime(row[0], dateformat)
	   home = row[2] if '@' in row[1] else row[5]
	   homesc = float(row[3] if '@' in row[1] else row[6])
	   away = row[5] if '@' in row[1] else row[2]
	   awaysc = float(row[6] if '@' in row[1] else row[3])
	   fmt = 'N' if not '@' in row[1] and not '@' in row[4] else ''
	   if stdev > 0:
    	       homesc += random.normalvariate(0, stdev)
	       awaysc += random.normalvariate(0, stdev)
	   game = [date, home, homesc, away, awaysc, fmt]
           games.append(game)
    return games
    
def processExceptions(games):
    for game in games:
        if game[0] < dt.datetime(2014, 3, 11): # pretend Embiid was not playing
            if game[1] == 'Kansas':
                game[2] = game[2] - 1.68
            elif game[3] == 'Kansas':
                game[4] = game[4] - 1.68

def computeRawRatings(games, div1, sensitivity, recencyAdj):
    seasonStart = games[0][0]
    seasonEnd = games[len(games)-1][0]
    seasonLen = float((seasonEnd - seasonStart).days)

    rawRatings=[]
    for [date, home, scoreHome, away, scoreAway, fmt] in games:
        # omit exhibitions and non-D1 games
        if fmt.find('E') == -1 and (home in div1 or away in div1):
            diff = scoreHome - scoreAway
            if fmt.find('N') != -1: # neutral site
                diff -= 2.5
            daysFromEnd = (seasonEnd - date).days
            diffAdj = sensitivity + recencyAdj * (seasonLen - daysFromEnd) / seasonLen
            rating = rh(diff * diffAdj)
            rawRatings.append([home, away, rating, scoreHome, scoreAway, date])
    return rawRatings

def getNameToIndex(rawRatings):
    teams=set()
    for [h,a,rh,hsc,asc,date] in rawRatings:
	teams.add(h)
	teams.add(a)

    teamlist = list(teams)
    teamlist.sort()
    nameidx={}
    for i,x in enumerate(teamlist):
	nameidx[x] = i
    return teamlist, nameidx

def computeAdjacency(rawRatings, teamlist, nameidx):
    adj = np.zeros([len(teamlist), len(teamlist)])

    for [h,a,rh,hsc,asc,date] in rawRatings:
        rh = min(0.99, max(0.01, rh))
        adj[nameidx[h], nameidx[a]] += rh
        adj[nameidx[a], nameidx[h]] += 1 - rh
    return adj

# Eigenvector power iteration
def eigenvaluePower(b0, a, iterCount):
    b = b0
    for i in range(iterCount):
	ab = np.dot(a,b)
	b = ab / LA.norm(ab)
    return b
    
# e.g. scoreNcaa(10, 20, 2, 1.5, 0)
def scoreNcaa(teamCount, iter, sensitivity, recencyAdj, stdev):
    # Replace these with paths to the files mentioned in my blog post.
    #basedir = '/Users/nathanbrixius/SkyDrive/Public/NCAA/'
    basedir = 'C:\\Users\\Nathan\\SkyDrive\\Public\\NCAA\\2014\\'
    ncaaCsv = basedir + 'ncaa2014massey.csv'
    teamsCsv = basedir + 'Teams2014.csv'

    div1 = getD1(teamsCsv)
    games = readGamesMassey(ncaaCsv, '%m/%d/%y', stdev) #, '%d-%b-%y' for Wolfe
    processExceptions(games)
    rawRatings = computeRawRatings(games, div1, sensitivity, recencyAdj)
    teamlist, nameidx = getNameToIndex(rawRatings)
    adj = computeAdjacency(rawRatings, teamlist, nameidx)
    b0=np.array([average_rating(rawRatings, t) for t in teamlist])
    #b0=np.ones(len(teamlist))
    b = eigenvaluePower(b0, adj, iter)
        
    # Sort teams and ratings in descending order
    namescore={}
    for i in range(len(teamlist)):
	namescore[teamlist[i]] = b[i]

    sorted_ns = sorted(namescore.iteritems(), key=operator.itemgetter(1))
    sorted_ns.reverse()

    final = []
    for [t,r] in sorted_ns:
        final.append([t,r])

    for i in range(teamCount):
        print(final[i])

    return final, namescore, rawRatings, teamlist

def all_games(rr, team):
    return [r[0:5] for r in rr if r[0] == team or r[1] == team]

def average_rating(rr, team):
    return np.average([(r[0] == team) * r[2] + (1-(r[0] == team))*(1-r[2]) for r in rr if r[0] == team or r[1] == team])

def ncaaMonteCarlo(trials, stdev):
    nss = {}
    final,ns,rr,tl = scoreNcaa(0, 20, 2, 1.5, stdev)
    for team in ns:
        nss[team] = [ ns[team] ]
    for trial in range(trials - 1):
        final,ns,rr,tl = scoreNcaa(0, 20, 2, 1.5, stdev)
        for team in ns:
            nss[team].append(ns[team])
    return nss

def ncaaMonteCarloStats(trials, stdev):
    teams = [
        "Florida","Albany NY","Colorado","Pittsburgh",
        "VA Commonwealth","SF Austin","UCLA","Tulsa",
        "Ohio St","Dayton","Syracuse","W Michigan",
        "New Mexico","Stanford","Kansas","E Kentucky",
        "Virginia","Coastal Car","Memphis","G Washington",
        "Cincinnati","Harvard","Michigan St","Delaware",
        "North Carolina","Providence","Iowa St","NC Central",
        "Connecticut","St Joseph's PA","Villanova","WI Milwaukee",
        "Arizona","Weber St","Gonzaga","Oklahoma St",
        "Oklahoma","N Dakota St","San Diego St","New Mexico St",
        "Baylor","Nebraska","Creighton","ULL",
        "Oregon","BYU","Wisconsin","American Univ",
        "Wichita St","Cal Poly SLO","Kentucky","Kansas St",
        "St Louis","NC State","Louisville","Manhattan",
        "Massachusetts","Tennessee","Duke","Mercer",
        "Texas","Arizona St","Michigan","Wofford"]
    nss = ncaaMonteCarlo(trials, stdev)
    result = []
    for team in teams:
       result.append([np.mean(nss[team]), np.std(nss[team])])
    return result