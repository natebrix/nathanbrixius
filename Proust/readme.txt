The text file contained in this directory is a compilation of all seven books of In Search of Lost Time by Marcel Proust, as originally translated by Charles Kenneth Scott Moncrieff.

This work is published under the Creative Commons License:
http://creativecommons.org/licenses/by-nc/3.0/

The text file was compiled from:

eBooks@Adelaide The University of Adelaide Library University of Adelaide South Australia 5005
http://ebooks.adelaide.edu.au/p/proust/marcel
